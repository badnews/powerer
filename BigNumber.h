//
// Created by Rav Chandra on 29/08/15.
//

#ifndef POWERER_BIGNUMBER_H
#define POWERER_BIGNUMBER_H

#include <vector>
#include <cstdint>

#define BN_BASE 100000000
#define BN_BASE_WIDTH 8

class BigNumber {
private:
    std::vector<uint32_t> num_array;
public:
    // constructors
    BigNumber();
    BigNumber(uint64_t value);

    // print some Big Numbers
    std::string show();

    // helpers
    void remove_leading_zeros();
    void expand_left(int size);
    void shift_left(int amount);
    BigNumber slice(int low, int high);

    // math operators
    friend BigNumber operator*(BigNumber a, const BigNumber &b);
    friend BigNumber karatsuba(BigNumber &a, BigNumber &b);
    friend BigNumber operator+(BigNumber a, const BigNumber &b);
    friend BigNumber operator-(BigNumber a, const BigNumber &b);
};

#endif //POWERER_BIGNUMBER_H
