# REQUIRES GCC 4.7+ !! C++11 SUPPORT
CC=g++
CFLAGS=-O2 -std=c++11
DIRS=bin build

all: dirs bin/powerer

dirs:
	mkdir -p $(DIRS)

.PHONY: dirs

bin/powerer: build/bignumber.o build/main.o
	$(CC) build/bignumber.o build/main.o -o bin/powerer

build/bignumber.o: BigNumber.cpp BigNumber.h
	$(CC) $(CFLAGS) -c BigNumber.cpp -o build/bignumber.o

build/main.o: main.cpp
	$(CC) $(CFLAGS) -c main.cpp -o build/main.o

clean:
	rm -rf $(DIRS)
