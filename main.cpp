#include <iostream>
#include "BigNumber.h"

using namespace std;


// recursive squaring exponentiation method
BigNumber fast_pow(BigNumber &a, const int b)
{
    if (b == 0) {
        return BigNumber(1);
    } else if (b % 2 == 1) {
        return a * fast_pow(a, b - 1);
    } else {
        auto p = fast_pow(a, b / 2);
        return p * p;
    }
}

// recursive squaring exponentiation method
BigNumber fast_pow_karatsuba(BigNumber &a, const int b)
{
    if (b == 0) {
        return BigNumber(1);
    } else if (b % 2 == 1) {
        return a * fast_pow(a, b - 1);
    } else {
        auto p = fast_pow(a, b / 2);
        return karatsuba(p, p);
    }
}

// naive exponentiation method
BigNumber pow(const BigNumber &a, const int value)
{
    BigNumber result = a;
    for (auto i = 0; i < value-1; i++)
    {
        result = result * a;
    }
    return result;
}


// helper to validate input
int read_int(int min, int max) {
    int number;
    cin >> number;
    if (cin.fail())
        throw "Invalid input";
    if (number > max)
        throw "Input greater than allowed max value";
    if (number < min)
        throw "Input less than allowed min value";
    return number;
}


int main() {
    int number;
    cout << "Enter a number (0-99999): ";
    try {
        number = read_int(0, 99999);
    } catch (const char *error_message) {
        cout << "Error: " << error_message << endl;
        return 1;
    }

    // string reverse the number
    auto rev_number_str = std::to_string(number);
    std::reverse(rev_number_str.begin(), rev_number_str.end());
    auto rev_number = std::stoi(rev_number_str);

    // friendly message to indicate to the user we are doing something
    cout << "Result " << number << "^" << rev_number << ": " << endl;

    // perform the calculation
    BigNumber a = BigNumber((uint64_t) number);
    auto c = fast_pow(a, rev_number);

    // display the result
    // oops, should really overload BigNum << operator, but we already had show()
    cout << c.show() << endl;

    return 0;
}
