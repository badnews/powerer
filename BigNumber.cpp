//
// Created by Rav Chandra on 29/08/15.
//

#include "BigNumber.h"
#include <iostream>
#include <sstream>
#include <iomanip>

// default constructor
BigNumber::BigNumber() {
}

// initialise with u64 value
BigNumber::BigNumber(uint64_t value) {
    while (value > 0) {
        num_array.push_back(value % BN_BASE);
        value /= BN_BASE;
    }
}

// print the digits
std::string BigNumber::show() {
    std::stringstream dec_result;
    auto it = num_array.rbegin();
    dec_result << (int) *it;
    it++;
    for (; it != num_array.rend(); it++) {
        dec_result << std::setfill('0') << std::setw(BN_BASE_WIDTH) << (int) *it;
    }
    return dec_result.str();
}

// clean up trailing (actually, leading) zeros, pretty lame, but... time
void BigNumber::remove_leading_zeros() {
    for (auto it = num_array.rbegin(); it != num_array.rend(); it++) {
        if (*it == 0)
            num_array.pop_back();
        else
            break;
    }
}

// zero pad at the "front"
void BigNumber::expand_left(int size) {
    auto padding = size - num_array.size();
    for (auto i = 0; i < padding; i++)
        num_array.push_back(0);
}

// shift operation, (i.e. zero pad at the "end")
void BigNumber::shift_left(int amount) {
    for (int i = 0; i < amount; i++) {
        num_array.insert(num_array.begin(), 0);
    }
}

// "long multiplication"
BigNumber operator*(BigNumber a, const BigNumber &b) {
    BigNumber result;
    uint64_t carry;
    int j;  // keep j around to write the final carry digit

    // preinitialise the result vector
    auto lhs_digits = a.num_array.size();
    auto rhs_digits = b.num_array.size();
    result.num_array = std::vector<uint32_t>(lhs_digits+rhs_digits, 0);

    // do the actual multiplication, use result as an accumulator
    for (int i = 0; i < rhs_digits; i++)
    {
        carry = 0;
        if (b.num_array[i] == 0) continue;
        for (j = 0; j < lhs_digits; j++)
        {
            // WARNING: explicit casts here to ensure we don't truncate on the multiplication
            uint64_t sum = carry + result.num_array[i+j] +
                    ((uint64_t) a.num_array[j] * (uint64_t) b.num_array[i]);
            result.num_array[i+j] = sum % BN_BASE;
            carry = sum / BN_BASE;
        }
        result.num_array[i+j] += carry;
    }

    // trim final result, could probaby actually precomute the exact number of digits required..
    // maybe with some clever logarithms?
    result.remove_leading_zeros();
    return result;
}

// Karatsuba's multiplication method
BigNumber karatsuba(BigNumber &a, BigNumber &b) {
    // do "normal" (long) multiplication for "small" operands
    int num_digits = (int) std::max(a.num_array.size(), b.num_array.size());
    if ((num_digits < 50) || (b.num_array.size() < 50)) {
        return a * b;
    }

    // split into pieces
    int middle_digit = num_digits / 2;
    if (b.num_array.size() < middle_digit + 1)
        b.expand_left(middle_digit * 2);
    BigNumber piece_b = a.slice(0, middle_digit);
    BigNumber piece_a = a.slice(middle_digit, (int) a.num_array.size());
    BigNumber piece_d = b.slice(0, middle_digit);
    BigNumber piece_c = b.slice(middle_digit, (int) b.num_array.size());

    // karatsuba steps
    BigNumber z0 = karatsuba(piece_b, piece_d);
    BigNumber a_plus_b = piece_a + piece_b;
    BigNumber c_plus_d = piece_c + piece_d;
    BigNumber z1 = karatsuba(a_plus_b, c_plus_d);
    BigNumber z2 = karatsuba(piece_a, piece_c);
    z2.remove_leading_zeros();

    // final summation of 3 constituent pieces
    BigNumber sum_products = (z1 - z0) - z2;
    z2.shift_left(2*middle_digit);
    sum_products.shift_left(middle_digit);
    return z2 + sum_products + z0;
}

// build new BigNum piece extracted from the current vector
BigNumber BigNumber::slice(int low, int high) {
    BigNumber result;
    for (int i = low; i < high; i++) {
        result.num_array.push_back(num_array[i]);
    }
    return result;
}

// addition of two BigNumbers, basically vector addition
BigNumber operator+(BigNumber a, const BigNumber &b) {
    BigNumber result;
    uint64_t carry = 0;
    auto num_digits = std::max(a.num_array.size(), b.num_array.size());
    for (auto i = 0; i < num_digits; i++) {
        uint32_t op_a = i < a.num_array.size() ? a.num_array[i] : 0;
        uint32_t op_b = i < b.num_array.size() ? b.num_array[i] : 0;
        carry = op_a + op_b + carry;
        result.num_array.push_back(carry % BN_BASE);
        carry /= BN_BASE;
    }
    if (carry > 0)
        result.num_array.push_back(carry);
    return result;
}

// subtraction of two BigNumbers, always assume a > b
// use the "borrow" method to pull in from the next digit if required
BigNumber operator-(BigNumber a, const BigNumber &b) {
    BigNumber result;
    uint64_t sum = 0;
    uint64_t operand_a, operand_b;
    for (int i = 0; i < a.num_array.size(); i++) {
        if (i >= b.num_array.size()) {
            sum = a.num_array[i];
        } else {
            operand_b = b.num_array[i];
            operand_a = a.num_array[i];
            if (operand_a < operand_b) {
                // borrow from next digit
                int j = i + 1;
                while (1) {
                    if (a.num_array[j] > 0) {
                        a.num_array[j] -= 1;
                        break;
                    } else {
                        a.num_array[j] = BN_BASE - 1;
                        j++;
                    }
                }
                operand_a += BN_BASE;
            }
            sum = operand_a - operand_b;
        }
        result.num_array.push_back(sum);
    }
    result.remove_leading_zeros();
    return result;
}
