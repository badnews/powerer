# Powerer: arbitrary-precision exponentiation #
------------------------------------------------

Ravi Chandra / ravi@chandra.cc / 31 August 2015

## Objective ##

Write a C++ application to perform exponentiation of large integers, *without using an external library like GMP, boost, etc*

## Build/run ##

This requires a compile that supports C++11, e.g. gcc 4.7+. I have tested using clang-602.0.53/LLVM 6.1.0 on OS X 10.10.5.

1) build progrm, binaries will be located in the `bin` directory

`$ make`

2) run it!

    $ bin/powerer
    Enter a number (0-99999): 123
    Result 123^321: 
    72367033806371673149109894141163778628811792657571658906010558390395870363798401744095280686155
    50773640492165707028496172182896059297790954263709889769722310262262856678765409132782545399159
    51402057014129613641887324089361978905536997158369515699998004319577692170067433210262575179327
    64164662319487914962533302741368207211189494615326552790667720411285474162636765168907211924134
    97337430449601963537666585855994173570392483646775691724799546958348746779152458215374452210759
    78652777981360800741614852804242740769310839944871117195622497025403628557129111322659662357543
    55353516703339043001506118520760359577737869472018617942120590873170710805078696371738906375721
    785723


## Implementation notes ##

- First started with creating a `BigNumber` class that can encapsulate *arbitrary precision integers* as a `std::vector` of native ints. Just define the base as 10 / decimal for now, since that is easy to reason and inspect as a human.

- Implemented the `*` operator using the "long multiplication" method as you would do with pen and paper.

- Add naive power function, works for `123**321`!! Crank up the test for `9999**9999`, works good, takes 2s. Now, the acid test: `99999**99999`.... running for 3 **minutes** and still no result :-( OK, so, there is more work to be done here..

- OK, using base 10 is not going to be very efficient, lets crank this up to some power of 2, like `2**16` => 65536. Modify the print function to print digits to verify the result. Result in 20s! *But, now I need to convert this number in base 65536 to base 10 :-/*

- Google solution: Horner's method. Implement it as an `::as_decimal()` member method. Something like:


```
BigNumber BigNumber::as_decimal() {
    BigNumber result(0, 10);
    auto factor = BigNumber(base, 10);
    for (auto i = num_array.size() - 1; i > 0; i--) {
        // down (add)
        result = result + BigNumber(num_array[i], 10);
        // right (multiply by base)
        result = result * factor;
    }
    // final add
    result = result + BigNumber(num_array[0], 10);
    return result;
}
```

(I removed this from the source code).

- OOOps, thats really not a good idea, the calculation can be performed pretty quickly but converting from base 65536 to base 10 is super SLOW with this approach. Scratch that.

- Try to stick with a base that is a power of 10 => try base 100000000. Then printing in base 10 should be relatively straightforward by adding padding to each digit! Performance for worst case input `99999**99999` is 20s! Pretty reasonable.

Stick with this base, because we want to be <= 2**32/2

- Maybe we can make this faster by not using a naive exponentiation method. Quick Google search suggests using a squaring exponentiation method. Give it a try.... 3.5s. Nice! This is now about as fast as Python (which I presume uses GMP under the hood):


```
RC in ~/ClionProjects/powerer on master*
$ time bin/powerer > /dev/null

real	0m3.442s
user	0m3.401s
sys	    0m0.018s

RC in ~/ClionProjects/powerer on master*
$ time python -c 'print 99999**99999' > /dev/null

real	0m4.190s
user	0m4.051s
sys	    0m0.034s
```

- Can we go faster? Hmm... Google seems to suggest that the *Karatsuba multiplication algorithm* is the defacto standard for this kind of thing. Is it really? Let's pick a language, how about Google Go and have a look at the source code. There is the `math.big.int` package for arbitrary-precision arithmetic. Let's find it, not too hard:
https://github.com/golang/go/blob/master/src/math/big/nat.go

- Aha, *Line 271*: `func karatsuba(z, x, y nat) {...`
Sure enough, the Karatsuba multpiplication method. I can't read or program in go so thats gibberish. Refer to wikipedia. Let's try it.

- Need a bunch more functionality in the `BigNumber` class to support this: addition and subtraction operators, and the ability to slice a `BigNumber` into pieces. Argh.
..Lots of coding and debugging ensues..

- Finally, it works! Correct result. And the performance.... 3.45s! No difference :-( That was a lot of effort for no performance improvement and a massive increase in complexity. Oh well. I suspect the problem is that (1) the algorithm needs more tuning with respect to the threshold value, (2) the input is still not large *enough*, and (3) in modern CPUs the saved multiplication operation is not necessarily *actually* and less than the extra add, sub and shifts.

## Verification ##

Normally I would create unit test for basic functions and operations, but due to a lack of time I have not done so. Instead, I've simply compared final results with those produced using `python`.
